<?php

/**
* Application.php
*/

namespace SolidStarter\SolidStarterFramework\Core;

class Application
{
	private $router;
	private $route;
	private $firewall;
	private $request;
	private $response;
	private $user;
	private $config;
	private $auth;
	private $dbManager;
	private $debug;
	private $prod;
	private $timers;
	private $modules;
	private $activeModule;

	function __construct(Request $req, array $modules, $prod=true, $debug=false, $timers=false)
	{
		$this->prod      = $prod;
		$this->debug     = $debug;
		$this->timers    = $timers;
		$this->request   = $req;
		$this->modules   = $modules;
		
		$this->config    = new Config();
		$this->config    = $this->config->build('../app/config/app.yml')->get('application');
		
		$this->dbManager = new DbManager($this);
		$this->dbManager->buildDbs();
		
		$this->auth      = new Auth($this);
		$this->auth->retrieveUser();
		
		$this->router    = new Router($this);
		$this->router->init();
		
		$this->firewall  = new Firewall($this);
	}

	public function handle(){
		/*
		 * Débord vérifier que l'utilisateur a le droit
		 * de consulter l'url demandée grâce au Firewall.
		 */

		$this->route        = $this->router->matchRoute();
		$code = (!($this->route INSTANCEOF Route)) ? $this->route : 200;
		$this->response = new Response("Default response text. If you see this message, you... No you just can't.", $code);
		if($code != 200)
			return $this->response->render();

		$this->activeModule = $this->route->getModule();
		$controller         = $this->activeModule->getNameSpace()."\\Controller\\".$this->route->getController().'Controller';
		$this->action       = $this->route->getAction().'Action';

		$this->controller   = new $controller($this);
		$this->response     = call_user_func_array(array($this->controller, $this->route->getAction().'Action'), $this->route->getVars());

		if(!($this->response INSTANCEOF Response)){
			echo("<h1 class='blink' style='color: #c00;'>ERROR: CONTROLLER MUST RETURN A RESPONSE OBJECT</h1>");
			$type = (get_class($this->response)) ? get_class($this->response) : "PLAIN TEXT";
			echo "<p>La réponse retournée est de type : <strong>".$type."</strong>";
		}

		return $this->response->render();
	}

	public function getRouter(){
		return $this->router;
	}
	public function getRoute(){
		return $this->route;
	}
	public function getUser(){
		return $this->auth->getUser();
	}
	public function getDbManager(){
		// Lors de la construction de DbManager, cette méthode est appelée.
		return (isset($this->dbManager)) ? $this->dbManager : false;
	}
	public function getAuth(){
		return $this->auth;
	}
	public function getRequest(){
		return $this->request;
	}
	public function getFirewall(){
		return $this->firewall;
	}
	public function getDebug(){
		return $this->debug;
	}
	public function getProd(){
		return $this->prod;
	}
	public function getTimers(){
		return $this->timers;
	}
	public function getModule($a){
		return $this->modules[$a];
	}
	public function getModules(){
		return $this->modules;
	}
	public function getActiveModule(){
		return $this->activeModule;
	}
	public function getConfig(){
		return $this->config;
	}

}

?>