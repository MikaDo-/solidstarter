<?php

/**
* User.php
*/
namespace SolidStarter\SolidStarterFramework\Core;

class BaseUser extends Model
{
	protected $table = "user";
	/*
	 * Si table non définie, nom de la classe appelée
	 */
	protected $prefix = false;
	/*
	 * Si prefix = false => pas de préfixe
	 * Si prefix vide => préfixe de la db
	 * Sinon préfix perso
	 */

	private $username;
	private $mail;
	private $password;
	private $salt;
	private $token;
	private $tokenExpiration;
	private $roles;
	private $enabled;

    /*
     * Peut être à caser dans UserManager...
     */
    public function genSalt($length = 9){
        $alphaNum = array('0'=>'a','1'=>'b','2'=>'c','3'=>'d','4'=>'e','5'=>'f','6'=>'g','7'=>'h','8'=>'i','9'=>'j','10'=>'k','11'=>'l','12'=>'m','13'=>'n','14'=>'o','15'=>'p','16'=>'q','17'=>'r','18'=>'s','19'=>'t','20'=>'u','21'=>'v','22'=>'w','23'=>'x','24'=>'y','25'=>'z', 26=>0, 27=>1,28=>2,29=>3,30=>4,31=>5,32=>6,33=>7,34=>8,35=>9);
        $salt ="";
        for ($i=0; $i < $length; $i++)
            $salt .= $alphaNum[floor(rand(0, count($alphaNum)-1))];
        return $salt;
    }

    /**
     * Gets the value of table.
     *
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Sets the value of table.
     *
     * @param mixed $table the table
     *
     * @return self
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Gets the User.php.
     *
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Sets the User.php.
     *
     * @param mixed $prefix the prefix
     *
     * @return self
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Gets the User.php.
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the User.php.
     *
     * @param mixed $username the username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Gets the value of mail.
     *
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Sets the value of mail.
     *
     * @param mixed $mail the mail
     *
     * @return self
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Gets the value of password.
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the value of password.
     *
     * @param mixed $password the password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the value of salt.
     *
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Sets the value of salt.
     *
     * @param mixed $salt the salt
     *
     * @return self
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Gets the value of token.
     *
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Sets the value of token.
     *
     * @param mixed $token the token
     *
     * @return self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Gets the value of tokenExpiration.
     *
     * @return mixed
     */
    public function getTokenExpiration()
    {
        return $this->tokenExpiration;
    }

    /**
     * Sets the value of tokenExpiration.
     *
     * @param mixed $tokenExpiration the tokenExpiration
     *
     * @return self
     */
    public function setTokenExpiration($tokenExpiration)
    {
        $this->tokenExpiration = $tokenExpiration;

        return $this;
    }

    /**
     * Gets the value of roles.
     *
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Sets the value of roles.
     *
     * @param mixed $roles the roles
     *
     * @return self
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Gets the value of enabled.
     *
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Sets the value of enabled.
     *
     * @param mixed $enabled the enabled
     *
     * @return self
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }
}

?>