<?php 

/**
* Response
*/
namespace SolidStarter\SolidStarterFramework\Core;

class Response
{
	
	function __construct($content, $code = 200)
	{
		$this->content = $content;
		$this->code = $code;
	}

	public function render(){
		if ($this->code != 200) {
			echo "<h1>APP CRASHED</h1><br><p>Status code: ".$this->code."</p>";
			exit($this->code);
		}
		echo $this->content;
	}

	public function getContent(){
		return $this->content;
	}

	public function getCode(){
		return $this->code;
	}
}

?>