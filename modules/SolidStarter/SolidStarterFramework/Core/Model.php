<?php

/**
* 
*/
namespace SolidStarter\SolidStarterFramework\Core;

class Model extends Component
{
	protected $id;
	protected $db;
	/**
	 * @var table
	 * Si table non définie, nom de la classe appelée
	 */
	protected $table;
	/**
	 * @var prefix
	 * Si prefix = false => pas de préfixe
	 * Si prefix vide => préfixe de la db
	 * Sinon préfix perso
	 */
	protected $prefix = false;
	protected $query;

	public function __construct(Application $app, $db = "main"){
		parent::__construct($app);
		$this->db = $this->app->getDbManager()->get($db);
		$this->loadModel();
	}

	public function setDb($db){
		$this->db = $this->app->getDbManager($db);
		$this->loadModel();
		return $this;
	}

	public function read($id){
		$q = "SELECT * FROM ".$this->table." WHERE id=".$id;
		$resultat=$this->db->getPDO()->query($q);
		$resultat->setFetchMode(\PDO::FETCH_ASSOC);

		foreach ($resultat->fetch() as $k => $v) {
			$this->$k = $v;
		}
		$resultat->closeCursor();

		return $this;
	}
	public function readBy($data=null){
		if(!$data)
			$data = array();
		$q = new Query($this->table);
		if(is_array($data))
			$q->where($data);
		
		$q->limit(1);

		$resultat = $this->db->getPDO()->query($q->build()->get());
		if(!$resultat)
			return false;
		$r = $resultat->fetch(\PDO::FETCH_ASSOC);
		foreach ($r as $k => $v)
			$this->{"set".ucfirst($k)}($v);
			
		return $this;
	}
	public function find($id){
		$rq = $this->db->getPDO()->prepare("SELECT * FROM ".$this->table." WHERE id= :id");
		$rq->execute(array("id"	=> $id));
		$resultat=$rq->fetch(\PDO::FETCH_ASSOC);
		$classname = get_called_class();
		$entity = new $classname();
		foreach ($resultat as $k => $v) {
			$entity->$k = $v;
		}
		$entity->db = $this->db;
		return $entity;
	}

	public function loadModel(){
		$classname = get_called_class();
		$tableName = (empty($this->table)) ? $classname : $this->table;
		if(empty($this->prefix))
			$prefix = $this->db->getPrefix();
		elseif($this->prefix === false){
			$prefix = "";
		}else{
			$prefix = $this->prefix;
		}
		$this->table = strtolower($prefix.$tableName);
		return $this;
	}

	public function findBy($data=null){
		if(!$data)
			$data = array(	'conditions' => '', 
							'limit'	=>	'',
							'order'	=>	'',
							);
		$q = new Query($this->table);
		if(is_array($data["conditions"]))
			$q->where($data["conditions"]);

		if(!empty($data["limit"]))$q->limit($data['limit']);
		if(!empty($data["order"]))$q->order($data['order'][0], $data['order'][1]);

		$result = $this->db->query($q->build()->get());
		if(!$result)
			return false;

		$classname = get_called_class();
		$entities = array();
		while ($resultat=$result->fetch(\PDO::FETCH_ASSOC)) {
			$newEntity = new $classname($this->getApp());
			foreach ($resultat as $k => $v) {
				//$newEntity->$k = $v;
				$newEntity->{"set".ucfirst($k)}($v);
			}
			$newEntity->db = $this->db;

			$entities[] = $newEntity;
		};
		return $entities;
	}

	public function findAll(){
		return $this->findBy();
	}

	public function save(){
		$vars = get_object_vars($this);
		unset($vars['id']);
		unset($vars['db']);
		unset($vars['table']);
		unset($vars['query']);

		if(empty($this->id)){
			$q = $this->insertion($vars);
			$this->id = $this->getAutoIncrement();
		}else{
			$q = $this->update($vars);
		}
		$this->db->getPDO()->exec($q);
		$result = $db->getPDO->query($q->build());

		return $this;
	}

	private function insertion($vars){
		$fields = "";
		$values = "";
		/*
		 * INSERT
		 */
		foreach ($vars as $k => $v) {
			$fields .= "`$k`, ";
			$values .= "\"$v\", ";
		}
		$fields = substr($fields, 0, -2);
		$values = substr($values, 0, -2);
		$qInsert = "INSERT INTO ".$this->table." (".$fields.") VALUES (".$values.")";

		return  $qInsert;
	}

	private function update($vars){
		$qUpdate = "UPDATE ".$this->table." SET ";

		foreach ($vars as $k => $v) {
			$qUpdate .= "`$k` = \"$v\", ";
		}
		$qUpdate = substr($qUpdate, 0, -2);
		$qUpdate .= " WHERE `id` =".$this->id;

		return $qUpdate;
	}

	public function getAutoIncrement(){
		$result = $this->db->getPDO()->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$result->setFetchMode(\PDO::FETCH_ASSOC);
		$ligne = $result->fetch();

		return $ligne['Auto_increment'];
	}

	public function delete(){
		$this->db->getPDO()->exec("DELETE FROM ".$this->table." WHERE id = ".$this->id);
		$this->__destruct();
	}

	public function __destruct(){
		$vars = get_object_vars($this);
		foreach ($vars as $k => $v) {
			$this->$k = NULL;
		}
	}

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of table.
     *
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Sets the value of table.
     *
     * @param mixed $table the table
     *
     * @return self
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Gets the value of query.
     *
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Sets the value of query.
     *
     * @param mixed $query the query
     *
     * @return self
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }
}

?>