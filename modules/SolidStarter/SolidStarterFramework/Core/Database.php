<?php

/**
* 
*/
namespace SolidStarter\SolidStarterFramework\Core;

class Database
{
	private $db;
	private $host;
	private $port;
	private $dbName;
	private $prefix;
	private $name;

	function __construct($pdo, $args){
		$this->host = $args['host'];
		$this->port = (isset($args['port'])) ? $args['port'] : 3306;
		$this->prefix = (isset($args['prefix'])) ? $args['prefix'] : "";
		$this->dbName = $args['dbName'];
		$this->db = $pdo;
		$this->name = $args['name'];
	}

	public function connect(){
		try{
			$this->db = new \PDO('mysql:host='.$this->host.';port='.$this->port.';dbname='.$this->dbName, $this->username, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		}catch(Exception $e){
	        echo 'Connexion impossible à la base de donnée !<br>'.$e->getMessage;
	        die();
		}
		return $this;
	}

	public function setDbName($n){
		$this->dbname = $n;
		$this->connect();
	}
	public function getPrefix(){
		return $this->prefix;
	}
	public function getPDO(){
		return $this->db;
	}
	public function query($param1){
		return $this->db->query($param1);
	}
	public function exec($param1){
		return $this->db->exec($param1);
	}
	public function prepare($param1){
		return $this->db->prepare($param1);
	}
}

?>