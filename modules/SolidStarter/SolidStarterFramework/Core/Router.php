<?php

/**
* Router
*/

namespace SolidStarter\SolidStarterFramework\Core;
use SolidStarter\SolidStarterFramework\Lib\Spyc\Spyc;

class Router extends Component
{
	private $parsedConfig;
	private $routes;
	private $regexBag;

	private $winner;

	public function init(){
	    $this->routes = $this->makeRoutes();
	}
	
    public function makeRegex(){
		$regexBag = array();
		$config = $this->getConfig()->get('routing')->get('modules');

		foreach ($config->get() as $km => $vm) 
			foreach ($vm->get() as $kr => $vr) {
				$regex = $vr->get('pattern')->get();
				if($vr->get('vars')){
					$vars = $vr->get('vars');
					foreach ($vars->get() as $kk => $vv)
						$regex = preg_replace("$\{".$kk."\}$", $vv->get(), $regex);
				}
				$regexBag[] = $regex;
			}
		return $regexBag;
	}
	
    public function getVars($route, $req){
		$matches = array();
		preg_match("$".$route["regex"]."$", $req, $matches);

		$vars = array();
		$k = 0;
		if (!empty($route["vars"])) {
			foreach ($route["vars"] as $kn => $v) {
				$vars[$kn] = $matches[$k+1];
				$k++;
			}
		}else{
			$vars = array();
		}
		return $vars;
	}
	
    public function makeRoutes(){
		$routes = array();
		$target = array();
		$config = $this->app->getConfig()->get('routing')->get('modules');
		$this->regexBag = $this->makeRegex();
		
		$k=0;
		foreach ($config->get() as $km => $vm) { // foreach module
			foreach ($vm->get() as $kr => $vr) { // foreach route
				$target = explode(":", $vr->get('target')->get());
				$varsAssoc = null;
				if($vars = $vr->get('vars')){
					foreach ($vars->get() as $kv => $vv) 
						$varsAssoc[$kv] = $vv->get();
				}

				$routesm[$kr]  = array(	"id"		 =>	$k,
									"name"       =>	$kr,
									"module"	 => $this->app->getModule($km),
									"pattern"    =>	$vr->get("pattern")->get(),
									"regex"      =>	$this->regexBag[$k],
									"controller" =>	$target[0],
									"action"     =>	$target[1],
									"vars"		 => $varsAssoc);
				$k+=1;
			}
			$routes[$km] = $routesm;
		}
		$this->debPrint("Routes", $routes);
		return $routes;
	}
	
    public function matchRoute(){
    	$result = null;
    	$req = $this->getRequest()->getPath();

		if($this->prod && $this->getConfig()->get('routing')->get('mysqlcache')->get() == "yes"){
			if($cacheRes = $this->cacheSeek($req))
				$this->winner = $this->buildFromCache($cacheRes, $req);
		}else{
			foreach ($this->routes as $km => $vm) {
				foreach ($vm as $kr => $vr) {
					$regex = "#^".$vr['regex']."$#U";
					if (preg_match($regex, $req)) {
						if($this->debug){ echo "<pre> OK : ".$kr." ".$vr["name"]." \t\t\t".$regex."</pre>";}
						$this->winner = $vr;
						if($this->checkWinner()){
							$result = array("controller"	=>	ucfirst($this->winner["controller"]),
								 		 	"action"		=>	$this->winner["action"],
											"vars"			=>	$this->getVars($this->winner, $req),
											"module"		=>	$this->winner["module"],
											"pattern"		=>	$this->winner["pattern"],
											"name"			=>	$vr["name"]
											);
						}else{
							$result = 409;
						}
						break;
					}else{
						$result = 404;
						if($this->debug){ echo "<pre> KO : ".$k." ".$v["name"]." \t\t\t".$regex."</pre>";}
					}
				}
			}
			$this->debPrint("Module/Contrôleur/Action", $result);
			$this->winner = is_int($result) ? $result : new Route($result) ;	

			if($this->prod && !is_int($this->winner))
				$this->logRoute($this->winner);			
		}
		return $this->winner;
	}
	
    public function checkWinner(){
		$this->debPrint("Winner route (if any)", $this->winner);
		$module       = $this->winner["module"];
		$this->module = $module;
		$controller   = $this->winner["controller"];
		$action       = $this->winner["action"];

		$controllerPath = "../modules/".$module->getNamespace()."/controller/".ucfirst($controller)."Controller.php";
	    if(file_exists($controllerPath)){
	        require_once($controllerPath);
	        $controller = ucfirst($module->getNameSpace())."\\Controller\\".ucfirst($controller)."Controller";
	        $controller = new $controller($this->app);
	        $methodName = $action."Action";
	        if (method_exists($controller, $methodName)) {
	        	return true;
	        }else{
	        	if($this->debug)echo "Action invalide ! :)";
	            return false;
	        }
	    }else{
	    	if($this->debug)echo "Contrôleur introuvable ! :)";
	        return false;
	    }
	}
	
	public function generateUrl($routeName, $params, $absolute=true){
		$route = $this->getConfig()->get('routing')->get('modules')->get($this->app->getActiveModule()->getName())->get($routeName);
		$url   = $route->get('pattern')->get();
		$vars   = $route->get('vars');

		if($vars)
		foreach ($vars->get() as $k => $v) {
			if(preg_match("#^".$v->get()."$#", $params[$k])){
				$url = preg_replace("$\{".$k."\}$", $params[$k], $url);
			}else{
				return false;
			}
		}

		if($absolute){
			$https = ($this->getConfig()->get('security')->get('https')->get() == "yes") ? "https" : "http";
			$url = $https."://".$_SERVER['HTTP_HOST'].$url;
		}else{
			$url = substr($url, 1);
		}
		return $url;

	}

	public function logRoute(){
		// ssc_ : SolidStarterCache
		$route = $this->winner;
		// Suite à optimiser.
		$vars = array('','','','','','','','','','');
		$q = "INSERT INTO ssc_routes VALUES('', ";
		$q .= '"'.$this->getRequest()->getPath().'", ';
		$q .= '"'.$route->getName().'", ';
		$kn=0;
		foreach ($route->getVars() as $k => $v) {
			$vars[$kn] = $v;
			$kn++;
		}
		foreach ($vars as $k => $v) {
			$q.= '"'.$v.'", ';
		}
		$q = substr($q, 0, -2).")";
		return $this->db->exec($q);
	}

	public function getWinner(){
		return $this->winner;
	}

	public function cacheSeek($req){
		$result = $this->db ->query('SELECT * FROM ssc_routes WHERE url="'.$req.'" LIMIT 1')
							->fetch(\PDO::FETCH_ASSOC);
		$this->debPrint("Cached route:", $result);
		if(!$result)
			return false;
		return $result;
	}

	public function buildFromCache($data, $req){
		foreach ($this->routes as $k => $v) {
			if ($v["name"] == $data["route"]) {
				$route = $this->routes[$k];
				$route["vars"] = $this->getVars($route, $req);
			}
		}
		$this->debPrint("Route", $route);
		return new Route($route);
	}

}

?>