<?php

/**
* Component
*/
namespace SolidStarter\SolidStarterFramework\Core;
class Component
{
	protected $app;
	protected $db;
	protected $router;
	protected $request;
	protected $firewall;
	protected $auth;
	protected $debug;
	protected $prod;

	public function __construct(Application $app){
		$this->app      = $app;
		if($this->app->getDbManager())
			$this->db   = $app->getDbManager()->getDb();
		$this->router   = $app->getRouter();
		$this->request  = $app->getRequest();
		$this->firewall = $app->getFirewall();
		$this->auth     = $app->getAuth();
		$this->debug    = $app->getDebug();
		$this->prod     = $app->getProd();
	}

	public function debPrint($name, $var){
		if ($this->debug) {
			echo "<h1>\$$name:</h1><pre>";print_r($var);echo "</pre>";
		}
	}

	public function devPrint($name, $var){
		if (!$this->prod) {
			echo "<h1>\$$name:</h1><pre>";print_r($var);echo "</pre>";
		}
	}

	public function generateUrl($routeName, $params, $absolute=true){
	    return $this->getRouter()->generateUrl($routeName, $params, $absolute);
	}

	/*
	 * Raccourcis sympathiques !
	 */
	public function post($a){
		return $this->request->post($a);
	}
	public function get($a){
		return $this->request->post($a);
	}
	public function session($a){
		return $this->request->post($a);
	}
	public function cookie($a){
		return $this->request->post($a);
	}

	public function getApp(){
		return $this->app;
	}
	public function getRouter(){
		return $this->app->getRouter;
	}
	public function getRequest(){
		return $this->app->getRequest();
	}
	public function getAuth(){
		return $this->app->getAuth();
	}
	public function getUser(){
		return $this->app->getAuth()->getuser();
	}
	public function getDb($dbName="main"){
		return $this->app->getDbManager()->get($dbName);
	}
	public function getFirewall(){
		return $this->app->getFirewall();
	}
	public function getConfig(){
		return $this->app->getConfig();
	}
}

?>