<?php

/**
* se contruit à partir de l'appli->sa requête
* son constructeur retourne l'utilisateur courant
*
* logout : déconnecte l'utilisateur
* login : défini l'utilisateur courant
* checkLogin : vérifie les informations de connexion et retourne soit un utilisateur soit false
* 
*/
namespace SolidStarter\SolidStarterFramework\Core;

class Auth extends Component
{
	private $token;
	private $user;

	public function login($user){
		if(!$this->checkLogin())
			return false;

		$token = md5($user->getSalt().md5(time()));
		$user->setToken();
		$user->setTokenExpire(time()+$this->app->config["login_token_ttl"]*60);
		return $token;
	}

	public function logout(){
		$this->user->setToken(null);
		$this->user = null;
		return true;
	}

	public function checkLogin(){
		$u = $this->post('username');
		$p = $this->post('password');
		$user = new BaseUser($this->getApp());
		$user->readBy(array("mail"=>"alexisbeaujet@gmail.com"));
		$pe = md5($user->getSalt().md5($p));


	}

	public function retrieveUser(){ // charge l'utilisateur courant depuis le token en session
		//$userToken = $this->getRequest()->session("token");
		$this->token = 4;
		if(!$this->token)
			return false;
		
		$user = new BaseUser($this->getApp()); // rappatrier la classe en charge des utilisateurs depuis la config

		$this->user = $user->findBy(array("conditions"=>array("token"	=>	$this->token)));

		//echo "<h1>\$this->user</h1><pre>";print_r($this->user);echo "</pre>";
	}
	public function getUser(){
		if(empty($this->user))
			$this->user = $this->retrieveUser();
		return $this->user;
	}
}

?>