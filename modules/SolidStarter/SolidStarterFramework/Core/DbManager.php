<?php

/**
* DbManager
*/
namespace SolidStarter\SolidStarterFramework\Core;
use SolidStarter\SolidStarterFramework\Lib\Spyc\Spyc;

class DbManager extends Component
{
	private $dbs;
	
	public function buildDbs(){
		foreach ($this->getConfig()->get('databases')->get() as $k => $v) {
			if (is_int($k)) continue;
			$port = $v->get('port')->get();
			$port = (preg_match("#^[\d+]+$#", $port)) ? $port : -1;
			$dsn = $v->get('driver')->get();
			$dsn .= ":dbname=".$v->get('name')->get().";";
			$dsn .= "host=".$v->get('host')->get().";";
			$dsn .= "port=".$v->get('port')->get();
			$con = new \PDO($dsn, $v->get('username')->get(), $v->get('password')->get());
			if ($con)
				$this->dbs[$k] = new Database($con, array(	"host" => $v->get('host')->get(), 
															"port" => $v->get('port')->get(), 
															"prefix" => $v->get('prefix')->get(), 
															"dbName" => $v->get('name')->get(), 
															"name" =>$v->get('name')->get()));
		}
	}

	public function get($dbname="main"){
		if (isset($this->dbs[$dbname]))
			return $this->dbs[$dbname];
		return false;
	}
}

?>