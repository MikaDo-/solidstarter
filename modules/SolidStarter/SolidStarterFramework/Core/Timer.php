<?php

namespace SolidStarter\SolidStarterFramework\Core;

/**
* Timer
*/
class Timer extends Component
{
	private $name;
	private $start;
	private $end;
	private $diff;
	private $precision;

	public function __construct(Application $app, $name, $precision=3){
		$this->start = microtime(true);
		$this->name = $name;
		$this->app = $app;
		$this->precision = $precision;
		return $this;
	}
	public function stop(){
		$this->stop = microtime(true);
		$this->diff = round($this->stop-$this->start, $this->precision);
		return $this;
	}

	public function display(){
		if ($this->app->getTimers())
			echo "Timer ".$this->name." : ".$this->diff."s.";
		return $this;
	}
}

?>