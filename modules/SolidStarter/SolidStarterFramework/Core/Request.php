<?php

/**
* Request.php
*/
namespace SolidStarter\SolidStarterFramework\Core;
class Request
{
	private $cookies;
	private $get;
	private $post;
	private $sessions;
	private $method;
	private $path;


	function __construct($path){
		$this->cookies = $_COOKIE;
		$this->get = $_GET;
		$this->post = $_POST;
		$this->sessions = $_SESSION;
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->path = $path;
	}

	public function getMethod(){
		return $this->method;
	}

	public function get($index){
		if(is_array($this->get) && isset($this->get[$index]))
			return $this->get[$index];
		return false;
	}
	public function post($index){
		if(is_array($this->post) && isset($this->post[$index]))
			return $this->post[$index];
		return false;
	}
	public function session($index){
		if(is_array($this->sessions) && isset($this->sessions[$index]))
			return $this->sessions[$index];
		return false;
	}
	public function getPath(){
		return $this->path;
	}


}

?>