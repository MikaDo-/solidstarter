<?php

namespace SolidStarter\SolidStarterFramework\Core;

/**
* Module
*/
class Module
{
	protected $name;
	protected $namespace;
	protected $shortname;
	
	function __construct($name){
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}

	public function getNamespace(){
		if (!isset($this->namespace)) {
			$reflect = new \ReflectionObject($this);
			$this->namespace = $reflect->getNamespaceName();
		}
		return $this->namespace;
	}
	public function getShortName(){
		if (!isset($this->shortname)) {
			$reflect = new \ReflectionObject($this);
			$this->shortname = $reflect->getShortName();
		}
		return $this->shortname;
	}
}

?>