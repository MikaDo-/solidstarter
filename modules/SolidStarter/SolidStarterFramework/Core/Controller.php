<?php

/**
*  Controller
*/
namespace SolidStarter\SolidStarterFramework\Core;
use SolidStarter\SolidStarterFramework\Lib\Spyc\Spyc;


class Controller extends Component
{
	private $twigEngine;
	private $controller;
	private $action;
	private $vars;
	private $module;
	private $pattern;
	private $route;

	function render($view, $params=null){
		$twig = $this->getTwig(); 
		if ($params == null) {
			$params = array();
		}
		try {
			return new Response($twig->render($view, $params), 200);
		} catch (Exception $e) {
			exit("Impossible de rendre la vue située dans views/".get_called_class()."/".$view.".<br>
				<pre>". $e->getMessage()."</pre>");
		}
	}
	function getRouter(){
		return $this->app->getRouter();
	}
	function getTwig(){
		$paths = array();
		$paths[] = ROOT."../modules/".$this->app->getActiveModule()->getNamespace()."/views/".ucfirst($this->app->getRoute()->getController())."/";
		$paths[] = ROOT."../app/layouts/";
		$layoutDir = ROOT."../modules/".$this->app->getActiveModule()->getNamespace()."/views/layout/";

		if(is_dir($layoutDir))
			$paths[] = $layoutDir;

		$loader = new \Twig_Loader_Filesystem($paths);
        $twig = new \Twig_Environment($loader);
		
		$twig->addFunction(new \Twig_SimpleFunction('asset', function ($path) {
			echo WEBROOT.$path;
		})); 
		$twig->addFunction(new \Twig_SimpleFunction('print_r', function ($a) {
		    print_r($a);
		}));
		$twig->addFunction(new \Twig_SimpleFunction('path', function ($route, $params) {
			$params = Spyc::YAMLLoad($params);
			echo $this->generateUrl($route, $params);
		}));
		
		return $twig;
	}

	function setModule($str){
		$this->module = $str;
	}

	public function initController(array $data){
		foreach ($data as $k => $v) {
			$this->$k = $v;
		}
	}

}

?>