<?php

namespace SolidStarter\SolidStarterFramework\Core;
use SolidStarter\SolidStarterFramework\Lib\Spyc\Spyc;

/**
* Config
* Configuration basée sur les ConfigNodes pouvant
* générer un arbre de ConfigNodes suivant un fichier
* de configuration YML.
*/
class Config extends ConfigNode
{
	public function build($path){
		$config = Spyc::YAMLLoad(file_get_contents(ROOT.$path));
		$this->parse($config);
		return $this;
	}
	public function parse($data){
		if(!is_array($data) && preg_match('#import:"(.+)"#U', $data, $path))
			$data = Spyc::YAMLLoad(file_get_contents(ROOT."../modules/".$path[1]));
		
		if (!is_array($data)) {
			$this->value($data);
			return $this;
		}
		foreach ($data as $k => $v) 
			$this->add($k)->get($k)->parse($v);
		return $this;
	}
}

?>