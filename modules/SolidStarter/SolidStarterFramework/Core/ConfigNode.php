<?php

namespace SolidStarter\SolidStarterFramework\Core;

/**
* ConfigNode
* L'idée est de posséder une config globale pour l'appli et de
* pouvoir récupérer les entrées de la config via cette fonction.
*
* Cette classe permet de former des noeuds qui - une fois mis en
* relation - formeront un arbre facile ) consulter et à générer
* de manière automatique à partir du fichier de configuration globale
* de l'application. En effet, celui-ci étant rédigé au format YAML,
* son parsage produit un tableau associatif simple à convertir en arbre.
*/

class ConfigNode
{
	protected $children; // 1 valeur ou collection de n ConfigNode
	protected $parent;
	
	function __construct($value=array(), $parent=NULL)
	{
		$this->children = array();
		$this->children[] = $value;
		$this->parent = $parent;
	}
	
	public function get($child =""){
		if ($child == "")
			return $this->children;
		if (preg_match("#/#", $child)) {
			$path = explode("/", $child);
			$res = $this;
			foreach ($path as $k => $v) {
				if(!empty($v)){
					if ($v == "..") {
						$res = $res->getParent();
					}else{
						$res = $res->get($v);
					}
				}
			}
			return $res;
		}else{
			if(isset($this->children[$child])){
				return $this->children[$child];
			}
		}
	}
	public function add($name, $value=NULL){
		unset($this->children[0]);
		$this->children[$name] = new static($value, $this);
		return $this;
	}
	public function set($name, $value){
		$this->children[$name] = $value;
	}
	public function value($value){
		$this->children = $value;
	}
	public function addNodes($array){
		$this->children = (is_array($this->children)) ? array_merge($this->children, $array) : $array;
		return $this;
	}
	public function getParent(){
		return $this->parent;
	}
	public function childrenCount(){
		return (is_array($this->children)) ? count($this->children) : false;
	}
}

?>