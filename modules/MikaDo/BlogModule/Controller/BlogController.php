<?php 

/**
* Blog controller
*/
namespace MikaDo\BlogModule\Controller;
use SolidStarter\SolidStarterFramework\Core\Controller;
use MikaDo\BlogModule\Model\Player;

class BlogController extends Controller
{
	function indexAction(){
		/*
		 * Faisons les test du modèle avec l'entité player
		 */
		$player = new Player($this->app);

		$players = $player->findAll();

		$url = $this->generateUrl("blog_article.test", array('id' => 4, "slug"	=>	"article-tres-intersessant", "category" => "science"), true);

		return $this->render("index.twig", array("name"	=>	$url));
	}
	public function testAction($id, $slug, $category){
		return $this->render("test.twig", array("id"	=>	$id, "slug"=>$slug, "category" => $category));
	}
	function articleAction($slug){
		//return $this->render("voir.twig", array("slug" => $args["slug"]));
		return "<pre>Slug: ".$slug."<pre>";
	}
	function addArticleAction($args){
		return $this->render("ajouter.twig", array("id" => $args["id"]));
	}
	function editArticleAction($args){
		return $this->render("editer.twig", array("id" => $args["id"]));
	}
	function delArticleAction($args){
		return $this->render("supprimer.twig", array("id" => $args["id"]));
	}
}


?>