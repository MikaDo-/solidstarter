# SolidStarter Framework

SolidStarter framework est un cadre de développement léger visant à offrir une simplicité d'utilisation se rapprochant de celle du framework CakePHP et l'organisation de Symfony2 sans pour autant être aussi lourd et donc exhaustif.

## Ce qu'il y a dans la boîte

SolidStarter Framework fourni une base de travail simple qui sera amenée à évoluer au fil du temps pour ajouter davantage de fonctionnalité tout en essayant d'être le plus rétrocompatible possible. 
#### Au menu
 - Une architecture MVC comprenant
     + Une organisation PSR-0
     + Un système de modules
     + Un routeur
         * facilement configurable par module (yml)
         * cache d'URL MySQL
     + Un gestionnaire de base de données configurable (yml)
     + Une abstraction de la base de données via un système de modèles simple à utiliser.
     + Un gestionnaire d'utilisateur flexibles et personnalisable
     + Un contrôleur d'accès simple à configurer (yml)
     

## Organisation

Tout comme Symfony, SolidStarter est basé sur un système de modules. 
Chaque module dispose d'une panoplie de contrôleurs, de modèles et de vues.
Chaque module peut disposer d'un layout indépendant ou utiliser le layout global de l'application (`/app/layouts`).

## Pour démarrer

Un petit wiki sera approvisionné pour fournir un guide un peu plus fourni.
En attendant voici comment commencer un projet.

### Créer un module
Pour commencer, créons un module avec son architecture de base et assurons nous de le déclarer auprès du Framework.

 - Créer le répertoire dans le dossier Modules sous la forme suivante : /Modules/[VotreNom]/[NomModule]Module/
 - Dans ce dossier, créer un fichier `[NomModule]Module.php` qui contiendra une classe vide héritant de `/SolidStarter/SolidStarterFramework/Core/Controller`
 - Créer les sous-dossiers de base
     + `Controller`
     + `Model`
     + `views`
         * `layout` (facultatif)
 - Ajouter ce modules dans la liste des modules renseignée dans /www/index.php : `'nomModule' => new /[VotreNom]/[NomModule]/[NomModule]Module(),`

### Configurer la base de données

La configuration des bases de données se fait dans le fichier `/app/config/databases.yml`.
SolidStarter Framework supporte un nombre illimité de bases de données. Ceci dit, il est impératif que l'une d'entre elles se nomme `main`. Il s'agit en effet de la base de donnée qui sera utilisée par défaut lors de la construction des modèles. Nous y reviendrons plus loin.
Voici en attendant un exemple de fichier de configuration:
```
main: # il DOIT y avoir une base main.
	driver: mysql
	host: localhost
	port: 3306
	username: user1
	password: nbvfde#54/d_
	name: db1
	prefix: mkd_

aux: # les autres bases peuvent être nommées librement
	driver: mysql
	host: localhost
	port: 3306
	username: user2
	password: _hY45Dc/
	name: db2
	prefix: mkd_ 
    # ce préfixe (facultatif) sera utilisé sur toutes les tables 
    # des entités utilisant cette base sauf indication contraire
    # (à préciser dans la définition du modèle, voir plus loin)

```

### Créer un contrôleur

Les contrôleurs doivent hériter de la classe `/SolidStarer/SoidStarterFrameword/Core/Controller` et être déclarés dans le namespace en accord avec l'emplacement du contrôleur c'est à dire: `[VotreNom]/NomModule]Module/Controller`.
Toutes les actions à l'intérieur se nommeront `[nomDeLAction]Action` et seront publiques.

voici donc un exemple de contrôleur basique:
```
<?php 
namespace BlogModule\Controller;
use SolidStarter\SolidStarterFramework\Core\Controller;

class BlogController extends Controller{
	function indexAction($id){
		return $this->render("index.twig", array("id"	=>	$id));
	}
}
?>
```

### Configurer le routing

SolidStarter Framework vous offre la possibité d'avoir un routing flexible et simple à configurer. Voici un exemple de configuration:

```
blog_index:
    pattern: "/blog"
    target: blog:index # contrôleur:action

blog_article.voir:
    pattern: "/blog/article-{slug}"
    target: blog:article
    vars:
        slug: ([a-zA-Z0-9\-]+) 
    # slug sera passé en paramètre à BlogController::articleAction($slug)
    # slug peut n'accepter qu'un certain type de données grâce
    # à une simple expression régulière.

blog_article.editer:
    pattern: "/blog/editer-{id}"
    target: blog:editArticle
    vars:
        id: (\d+)
```
Les motifs d'url (patterns) représentent ce qui sera tapé dans la barre d'adresse du navigateur à la suite du nom de domaine de l'application. Si deux motifs conviennent pour une URL donnée, alors la route lue en premier sera choisie par le routeur.

Il est important de noter que les noms des routes seront réutilisés pour générer les URLs depuis les contrôleurs ou les vue. Il est donc conseillé de suivre une certaine logique.
Par exemple: `module_controleur.action` Notez aussi que **deux routes dans deux modules distincts ne peuvent pas porter le même nom !** Si c'est le cas, alors la route lue en premier sera la route choisie.

### Configurer le gestionnaire d'utilisateurs

En développement.

### Configurer le contrôleur d'accès

En développement.

## Bibliothèques externes

Pour fonctionner (parser les config yaml, templating), SolidStarter Framework utilise plusieurs bibliothèques libres.

 - Twig - moteur de template intuitif et puissant - [Site éditeur](http://twig.sensiolab.org/)
 - Spyc - parseur YML pour des fichiers de configurations simples à rédiger - [Site éditeur](https://code.google.com/p/spyc/)

## Licence