<?php

    use SolidStarter\SolidStarterFramework\Core\Application;
    use SolidStarter\SolidStarterFramework\Core\Request;

    session_start();

    define("WEBROOT", str_replace("index.php", "", $_SERVER['SCRIPT_NAME']));
    define("ROOT", str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']));
    define("_DEBUG", false);
    define("_PROD", false);
    define("_TIMERS", true);


    require_once("../core/core.php");

    $modules = array(   'MikaDo\Blog'   => new \MikaDo\BlogModule\BlogModule('MikaDo\Blog'),
                        'MikaDo\Sample' => new \MikaDo\SampleModule\SampleModule('MikaDo\Sample'),
                    );

    $app = new Application(new Request("/".$_GET['solidstarterpath']), $modules, _PROD, _DEBUG, _TIMERS);
    $app->handle();

?>