﻿<?php 

    require('../modules/SolidStarter/SolidStarterFramework/Lib/Twig/Autoloader.php');
	Twig_Autoloader::register();

    /*
     * Autoload vite fait
     */
    spl_autoload_register(function($classname){
        if(!class_exists($classname, false))
            include("../modules/".$classname.".php");
    });

?>